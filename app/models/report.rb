class Report < ApplicationRecord
     validates :event, presence: true
     validates :report, presence: true
     validates :by, presence: true
     validates :desciption, presence: true
end
