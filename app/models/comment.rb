class Comment < ApplicationRecord
  belongs_to :register , optional: true
  belongs_to :event , optional: true
end
