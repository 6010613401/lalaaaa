class Event < ApplicationRecord
    validates :title, presence: true
    validates :category, presence: true
    validates :description, presence: true
    validates :phone, presence: true 
    belongs_to :register
    acts_as_votable
    has_many :comments
end
