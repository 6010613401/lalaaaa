require 'rails_helper'

RSpec.describe "registers/new", type: :view do
  before(:each) do
    assign(:register, Register.new(
      :email => "email@123.com",
      :username => "user",
      :password => "secret",
      :password_confirmation => "secret",
      :phone => "088"
    ))
  end

  it "renders new register form" do
    render

    assert_select "form[action=?][method=?]", registers_path, "post" do

      assert_select "input[name=?]", "register[email]"

      assert_select "input[name=?]", "register[username]"

      assert_select "input[name=?]", "register[password]"

      assert_select "input[name=?]", "register[phone]"
    end
  end
end
