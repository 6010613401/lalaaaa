require 'rails_helper'

RSpec.describe "registers/index", type: :view do
  before(:each) do
    assign(:registers, [
      Register.create!(
      :email => "email1@123.com",
      :username => "user1",
      :password => "secret",
      :password_confirmation => "secret",
      :phone => "0985218956"
      ),
      Register.create!(
      :email => "email@123.com",
      :username => "user",
      :password => "secret",
      :password_confirmation => "secret",
      :phone => "0985218956"
      )
    ])
  end

  it "renders a list of registers" do
    render
    assert_select "tr>td", :text => "Email".to_s, :count => 0
    assert_select "tr>td", :text => "Username".to_s, :count => 0
    assert_select "tr>td", :text => "".to_s, :count => 0
    assert_select "tr>td", :text => "Phone".to_s, :count => 0
  end
end
