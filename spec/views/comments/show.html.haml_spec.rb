require 'rails_helper'

RSpec.describe "comments/show", type: :view do
  before(:each) do
    @comment = assign(:comment, Comment.create!(
      :event_id => 2,
      :body => "Text",
      :register_id => 1
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(2)
    expect(rendered).to match("Text")
    expect(rendered).to match(1)
  end
end
