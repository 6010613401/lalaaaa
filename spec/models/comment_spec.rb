require 'rails_helper'

RSpec.describe Comment, type: :model do
it "can't be saved without a user" do
  event = Event.new
  event.user = nil
  expect { event.save!(validate: false) }.to raise_error ActiveRecord::NotNullViolation
  end
end
